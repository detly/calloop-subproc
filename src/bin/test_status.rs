//! Test binary for `calloop_subproc` package. Call it with a single argument,
//! and it will use it as its exit status (for testing things that depend upon
//! exit status). If the exit status can't be parsed as a signed integer, the
//! exit status will be `-255`.
//!
fn main() {
    let status = std::env::args()
        .nth(1)
        .as_deref()
        .map(str::parse)
        .transpose()
        .ok()
        .flatten()
        .unwrap_or(-255);

    std::process::exit(status);
}
