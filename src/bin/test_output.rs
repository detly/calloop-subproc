//! Test binary for `calloop_subproc` package. It will print the following lines
//! to stdout with a 100ms delay in between:
//!
//!     1 (one)
//!     2 (two)
//!     3 (three)
//!
//! Then it will block until killed.

const DELAY: std::time::Duration = std::time::Duration::from_millis(100);

fn main() {
    use std::thread::{park, sleep};

    println!("1 (one)");
    sleep(DELAY);
    println!("2 (two)");
    sleep(DELAY);
    println!("3 (three)");

    loop {
        park()
    }
}
