//! Test binary for `calloop_subproc` package. It will print the following lines
//! to stdout with no delay in between:
//!
//!     1 (one)
//!     2 (two)
//!     3 (three)
//!
//! Then it will exit.

fn main() {
    println!("1 (one)");
    println!("2 (two)");
    println!("3 (three)");
}
