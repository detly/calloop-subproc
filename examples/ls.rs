fn main() {
    use calloop::{EventLoop, LoopSignal};
    use calloop_subproc::SubprocListen;

    let ls_command = calloop_subproc::Command::new("ls").with_args(["-a"]);
    let listener = SubprocListen::new(ls_command).unwrap();

    let mut event_loop: EventLoop<LoopSignal> = EventLoop::try_new().unwrap();

    event_loop
        .handle()
        .insert_source(listener, |event, _, stopper| {
            // What kind of event did we get this time?
            let msg = match event {
                // The subprocess was just started.
                calloop_subproc::ListenEvent::Start => "Subprocess started".to_owned(),
                // We got a line of output from the subprocess.
                calloop_subproc::ListenEvent::Line(line) => format!("Output: {}", line),
                // The subprocess ended.
                calloop_subproc::ListenEvent::End(res) => {
                    // Since the subprocess ended, we want to stop the loop.
                    stopper.stop();

                    // Show why the subprocess ended.
                    match res {
                        Ok(()) => "Subprocess completed".to_owned(),
                        Err(error) => format!("Subprocess error: {:?}", error),
                    }
                }
            };

            // Print our formatted event.
            println!("{}", msg);

            // This callback must return true if the subprocess should be
            // killed. We want it to run to completion, so we return false.
            false
        })
        .unwrap();

    event_loop
        .run(None, &mut event_loop.get_signal(), |_| {})
        .unwrap();
}
