mod common;

#[test]
fn test_chain_single_success() {
    common::init_logging();
    let commands = [common::test_status_cmd(0)];

    let create = move || calloop_subproc::SubprocChain::new(commands, None).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(event, Some(Ok(()))));
}

#[test]
fn test_chain_single_failure() {
    common::init_logging();
    let commands = [common::test_status_cmd(1)];

    let create = move || calloop_subproc::SubprocChain::new(commands, None).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(
        event,
        Some(
            Err(
                calloop_subproc::ErrorEvent::SubprocError(err)
            )
        ) if err.code() == Some(1)
    ));
}

#[test]
fn test_chain_multiple_success() {
    common::init_logging();

    // We can't test from the "other side" to see whether these all got called.
    // For now, assume they do.
    let commands = [
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
    ];

    let create = move || calloop_subproc::SubprocChain::new(commands, None).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(event, Some(Ok(()))));
}

#[test]
fn test_chain_multiple_success_fail() {
    common::init_logging();

    // We can't test from the "other side" to see whether these all got called.
    // For now, assume they do.
    let commands = [
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(1),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
    ];

    let create = move || calloop_subproc::SubprocChain::new(commands, None).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(
        event,
        Some(
            Err(
                calloop_subproc::ErrorEvent::SubprocError(err)
            )
        ) if err.code() == Some(1)
    ));
}

#[test]
fn test_chain_multiple_success_with_cleanup_failure() {
    common::init_logging();

    let commands = [
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
    ];

    let cleanup = common::test_status_cmd(1);

    let create = move || calloop_subproc::SubprocChain::new(commands, Some(cleanup)).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(event, Some(Ok(()))));
}

#[test]
fn test_chain_multiple_success_with_failure_and_cleanup() {
    common::init_logging();

    let commands = [
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(1),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
    ];

    let cleanup = common::test_status_cmd(0);

    let create = move || calloop_subproc::SubprocChain::new(commands, Some(cleanup)).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(
        event,
        Some(
            Err(
                calloop_subproc::ErrorEvent::SubprocError(err)
            )
        ) if err.code() == Some(1)
    ));
}

#[test]
fn test_chain_multiple_success_with_failure_and_cleanup_failure() {
    common::init_logging();

    let commands = [
        common::test_status_cmd(0),
        common::test_status_cmd(0),
        common::test_status_cmd(1),
        common::test_status_cmd(0),
        common::test_status_cmd(0),
    ];

    let cleanup = common::test_status_cmd(2);

    let create = move || calloop_subproc::SubprocChain::new(commands, Some(cleanup)).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First and only event should be the exit status of the chain.
    let event = runner.next();
    runner.send(());

    assert!(matches!(
        event,
        Some(
            Err(
                calloop_subproc::ErrorEvent::SubprocError(err)
            )
        ) if err.code() == Some(1)
    ));
}
