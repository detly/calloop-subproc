use calloop_subproc::ListenEvent;
mod common;

#[test]
fn test_listen_one_line() {
    common::init_logging();

    let command = common::test_output_cmd();

    let create = move || calloop_subproc::SubprocListen::new(command).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First event should be the start event.
    let event = runner.next();
    assert!(matches!(event, Some(ListenEvent::Start)));

    // Send false to continue.
    runner.send(false);

    // First line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "1 (one)"));

    // "Return" true to kill the process
    runner.send(true);

    let event = runner.next();
    assert!(matches!(event, Some(ListenEvent::End(Err(_)))));

    // Should be ignored by the actual source, but our test loop needs it.
    runner.send(true);
}

#[test]
fn test_listen_three_lines() {
    common::init_logging();

    let command = common::test_output_cmd();

    let create = move || calloop_subproc::SubprocListen::new(command).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First event should be the start event.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Start)));

    // "Return" false to continue the process
    runner.send(false);

    // First line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "1 (one)"));

    // "Return" false to continue the process
    runner.send(false);

    // Second line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "2 (two)"));

    // "Return" false to continue the process
    runner.send(false);

    // Third line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "3 (three)"));

    // "Return" true to kill the process.
    runner.send(true);

    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::End(Err(_)))));

    // Should be ignored by the actual source, but our test loop needs it.
    runner.send(true);
}

#[test]
fn test_listen_three_lines_fast() {
    common::init_logging();

    let command = common::test_output_asap_cmd();

    let create = move || calloop_subproc::SubprocListen::new(command).unwrap();

    let mut runner = common::SpawnedCalloop::new(create);

    // First event should be the start event.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Start)));

    // "Return" false to continue the process
    runner.send(false);

    // First line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "1 (one)"));

    // "Return" false to continue the process
    runner.send(false);

    // Second line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "2 (two)"));

    // "Return" false to continue the process
    runner.send(false);

    // Third line of text.
    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::Line(line)) if line == "3 (three)"));

    // "Return" false to continue the process
    runner.send(false);

    let event = runner.next();

    assert!(matches!(event, Some(ListenEvent::End(Ok(())))));

    // Should be ignored by the actual source, but our test loop needs it.
    runner.send(true);
}
